Source: openvlbi
Section: misc
Priority: optional
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13)
	, cmake
 	, cmake-data
	, libindi-dev (>= 1.8.8)
	, zlib1g-dev
	, libfftw3-dev
	, libnova-dev
	, libcfitsio-dev
	, libpng-dev
	, libjpeg-dev
 	, doxygen
Standards-Version: 4.7.0
Homepage: https://iliaplatone.github.io/OpenVLBI/
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian-astro-team/openvlbi
Vcs-Git: https://salsa.debian.org/debian-astro-team/openvlbi.git

Package: libopenvlbi3t64
Provides: ${t64:Provides}
Replaces: libopenvlbi3
Breaks: libopenvlbi3 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: OpenVLBI Correlator library
 OpenVLBI is an open source library for astronomical interferometers.
 with OpenVLBI astronomers can join together more telescopes to observe
 celestial objects at great resolution.
 OpenVLBI can be used with radio antennas or optical sensors.
 A test and multi-contextual client/server applications are included into
 openvlbi-bin, and libopenvlbi contains the correlator library.
 An INDI client is included into openvlbi-bin which connects to one or more
 INDI servers that observe in realtime.
 .
 This package contains the library.

Package: libopenvlbi-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
	, libopenvlbi3t64 (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: OpenVLBI Correlator library - development files
 OpenVLBI is an open source library for astronomical interferometers.
 with OpenVLBI astronomers can join together more telescopes to observe
 celestial objects at great resolution.
 OpenVLBI can be used with radio antennas or optical sensors.
 A test and multi-contextual client/server applications are included into
 openvlbi-bin, and libopenvlbi contains the correlator library.
 An INDI client is included into openvlbi-bin which connects to one or more
 INDI servers that observe in realtime.
 .
 This package contains the development files.

Package: libopenvlbi-doc
Section: doc
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: OpenVLBI Correlator - documentation
 OpenVLBI is an open source correlator for astronomical interferometers.
 with OpenVLBI astronomers can join together more telescopes to observe
 celestial objects at great resolution.
 OpenVLBI can be used with radio antennas or optical sensors.
 .
 This package contains documentation.

Package: openvlbi-bin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
	, libopenvlbi3t64 (= ${binary:Version})
Description: OpenVLBI Correlator library - client applications
 OpenVLBI is an open source library for astronomical interferometers.
 with OpenVLBI astronomers can join together more telescopes to observe
 celestial objects at great resolution.
 OpenVLBI can be used with radio antennas or optical sensors.
 A test and multi-contextual client/server applications are included into
 openvlbi-bin, and libopenvlbi contains the correlator library.
 An INDI client is included into openvlbi-bin which connects to one or more
 INDI servers that observe in realtime.
 .
 This package contains the client application.

Package: libopendsp3t64
Provides: ${t64:Provides}
Replaces: libopendsp3
Breaks: libopendsp3 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: OpenDSP signal processing library
 OpenDSP is an open source signal processing library.
 It contains methods and routines for signal processing
 in imaging and radio astronomy.
 .
 This package contains the library.

Package: libopendsp-dev
Section: libdevel
Architecture: any
Depends: libopendsp3t64 (= ${binary:Version})
	, ${shlibs:Depends}, ${misc:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: OpenDSP signal processing library - development files
 OpenDSP is an open source signal processing library.
 It contains methods and routines for signal processing
 in imaging and radio astronomy.
 .
 This package contains the development files.

Package: openvlbi-data
Section: libs
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: OpenVLBI correlator - data
 OpenVLBI is an open source correlator for astronomical interferometers.
 with OpenVLBI astronomers can join together more telescopes to observe
 celestial objects at great resolution.
 OpenVLBI can be used with radio antennas or optical sensors.
 .
 This package contains data files.

Package: openvlbi
Architecture: all
Depends: openvlbi-bin (>= ${source:Version})
	, openvlbi-data (>= ${source:Version})
	, libopendsp3t64 (>= ${source:Version})
	, libopendsp-dev (>= ${source:Version})
	, libopenvlbi3t64 (>= ${source:Version})
	, libopenvlbi-dev (>= ${source:Version})
	, ${shlibs:Depends}, ${misc:Depends}
Description: correlator for OpenVLBI
 OpenVLBI is an open source correlator for astronomical interferometers.
 with OpenVLBI astronomers can join together more telescopes to observe
 celestial objects at great resolution.
 OpenVLBI can be used with radio antennas or optical sensors.
 This package includes test and multi-contextual client/server applications,
 the development and runtime libraries.
 .
 This is a metapackage to install all openvlbi packages.
